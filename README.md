# PyPacker

Run and compile you java projects automatically!

## Description

Compile mass amounts of files iterably into a target directory.

Auto run and compile.

## Changelog

 - optimized pack command greatly
 - added "autoRetry" option in `config.json`

## To Do

 - improve readability
 - increase usability
 - increase error catching
 - increase debug possibility (tells the user when something goes wrong)

## Support

no support, your on your own :) ...just kidding! just ask and I'll help

## Authors and Acknowledgment

I am currently the only author.

## Project status

Early development.

## Installation

git clone the repo, move `p.py` and `packer.py` to your root directory, run `$ python3 p.py init` or `$ ./p.py init` to create the config. after this you should be good to go!

## Usage

See [docs.md](docs.md)

## Contributing

Any PR's / bug reports are welcome. Even tiny ones. I appreciate any and all help

## License

Please keep this software open source. If you fork the project or take certain parts of it, thats fine, but give credit where credit is due.

Please do not close-source your forks of this.

## Hi

Payton was here.
