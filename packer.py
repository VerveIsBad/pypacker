#!/usr/bin/env python3


import os
import json
import sys
import subprocess


# variables
RESET = "\u001b[0m"
PACKER = f"[\033[1;36;40mPACKER{RESET}]"
BRIGHT_GREEN = "\033[1;32;40m"
BRIGHT_RED  = "\033[1;31;40m"
FATAL = f"[\033[1;31;40mFATAL{RESET}]"

source = ""
class_path = ""
entry = ""


# Output
def output(message: str , fatal: bool = False) -> None:
    if fatal:
        print(f"{FATAL} {message}")
    else:
        print(f"{PACKER} {message}")


# Run
def run(custom: list = None) -> None:
    """
        Run files.
        Custom = Non-entry point file.
        defaults to entry ( found in ./config.json )
    """
    global class_path, entry

    if custom is None: # unix specific command
        os.system(f"cd {class_path} && java -cp {class_path}: {entry}")
    else:
        os.system(f"cd {class_path} && java -cp {class_path}: {custom}")


# Compile (Pack) Files
def pack(custom_files: list = None):
    global source, class_path

    if type(custom_files) is str and custom_files is not None:
        custom_files = list(custom_files.split(" "))
    
    elements = 0
    count = 0
    
    """
        Compiles .java files at the given path
        ALL .class files will be directed to ./target/
        Path = path name. E.X src (do not use ./src/ )
    """

    files_that_packed = 0
    
    """
        creates a dict containg the file structure
        this can be used to iterate through the file tree and compile
        each file
        Example dict:
            {
                './src/': ['Main.java'], 
                './src/anotherDir': ['readMe.txt', 'Random.java'], 
                './src/anotherAnotherdir': ['Sort.java']
            }
    """
    
    files_that_gave_error = []

    if custom_files is None:
        files_dict = {}
        for dirpath, dirnames, files in os.walk(source):
            files_dict[dirpath] = [file for file in files if file.endswith('.java')]  # filter non-java files

        temp = list(files_dict.values())
        
        for i in range(len(temp)):
            for j in range(len(temp[i])):
                elements += 1  # grab number of elements in the dict
            
        for path, files in files_dict.items():
            for file in files:
                count += 1
                output(f"packing ({count}/{elements})\r")  # progress
                cmd = f"javac -d {class_path} -cp {class_path} {path}/{file}"  # create command
                
                code, reason = subprocess.getstatusoutput(cmd)  # attempt to compile to class_path                    
                
                if code != 0: # fail
                    files_that_gave_error.append(f"{path}/{file}")
                else:
                    files_that_packed += 1

    elif custom_files is not None:
        for file in custom_files:
            count += 1
            output(f"packing ({count}/{len(custom_files)})\r")  # progress
            
            # assumes the file is like a cmd
            code, reason = subprocess.getstatusoutput(f"javac -d {class_path} -cp {class_path} {file}")
            
            if code != 0:
                files_that_gave_error.append(file)
            else:
                files_that_packed += 1


    if len(files_that_gave_error) != 0:
        return False, files_that_gave_error, [files_that_packed, len(files_that_gave_error)], reason
    
    return True, None, [files_that_packed, len(files_that_gave_error)], None   


# TODO: Optimize compilations
