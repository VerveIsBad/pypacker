# Documentation

## Important

This project is in its Early-Development stage, and there WILL be bugs.

If you find a bug that needs fixing, please open a issue [here](https://gitlab.com/VerveIsBad/pypacker)

## Features
 - Compile all of your .java files into class files contained in a ./classes/ (or whatever you choose :D ) directory
 - Run your entry point with a single command
 - Compile and run your files with ease.
 - Automatically retries failed compimations.

## Usage

To get started, git clone the project [here](https://gitlab.com/VerveIsBad/pypacker)

Run `$ python3 p.py init` and walk through the setup script.

This will create a `config.json` that can be changed at any time

## Packing Files

To pack your files, make sure `p.py` and `packer.py` are inside of your root directory, beside your source folder.


Use the command `$ python3 p.py pack`.

`p.py` will call the `packer.pack()` method, and `packer.py` will iterate through your `source` directory, creating a dictionary containing the file structure that can be read. (see lines 54 - 72 for a explanation)

The packer will then loop through the ( path, files ) key/value pairs inside, and compile all of the .java files to your `classPath` (see `config.json`)

> Note: If one or more of the files fail to compile, a `subprocess.CalledProcessError` is called, `packer` will catch the error and save the file that failed to compile into a list. it will do this for all failed files. 

> Failed complimation will not stop the program from trying to compile the rest of the files.

After the rest of the files have been compiled, it will return the failed file list. `p.py` will then call `packer.pack()` and give it the list of failed files, and try to compile again. it it fails 3 more times the program will stop and tell the user. 

## Running Files

Use `$ python3 p.py run <fileName>`

This command does not require a given file name, but one can be provided. When calling without a given file name, it will run your entry point program ( see `config.json` ). it will NOT catch errors from the program execution(s), it simply runs the file using the `java` command.

## _fin_

Thanks for reading! There isn't much available right now, so make sure you check back for any updates!
