#!/usr/bin/env python3


"""
This programs sole purpose is to clean up packer.py
this is where the user will interact with it
"""


import packer
import sys
import os
import json


# Variables
RESET = "\u001b[0m"
PACKER = f"[\033[1;36;40mPACKER{RESET}]"
BRIGHT_GREEN = "\033[1;32;40m"
BRIGHT_RED  = "\033[1;31;40m"
FATAL = f"[\033[1;31;40mFATAL{RESET}]"

auto_retry = ""


# Errors
def pack_failed(report: list, failed_files: list, reason: str) -> None:
    output("could not pack files (try `python p.py pack` to re-compile everything)", True)
    output(f"Report:    {BRIGHT_GREEN}PACKED{RESET} {report[0]}  {BRIGHT_RED}FAILED{RESET} {report[1]}", True)
    output(f"Failed files: {failed_files}", True)
    output(f"Reason: {reason}", True)


def invalid_args(args: list) -> None:
    output("Invalid args, please use one of the following: 'run', 'pack', 'init' ", True)
    output(f"Given args: {args}", True)
    sys.exit(1)


# Config
def get_config() -> dict:
    global auto_retry

    f = open("config.json")
    config = json.load(f)

    auto_retry = config["autoRetry"]

    packer.source = config["source"]
    packer.entry = config["entry"]
    packer.class_path = config["classPath"]

    return config  # optional


def create_config() -> None:
    output("(please do not include .java as programs execute from .class files)")
    
    user_entry = input(f"{PACKER} entry point: (Default: Main) ")
    user_class_path = input(f"{PACKER} Location of class path: (Default: ./classes/) ")
    user_source = input(f"{PACKER} Source files: (Default: src) ")  
    user_auto_retry = input(f"{PACKER} Auto retry on pack fail: (Default: True) ")

    # Create object
    config = {
        "entry": "Main" if user_entry == "" else user_entry,
        "classPath": "./classes/" if user_class_path == "" else user_class_path,
        "source": "./src/" if user_source == "" else user_source,
        "autoRetry": True if user_auto_retry == "" else user_source
    }

    with open("config.json", "w") as f:  # create file (or re-create / overwrite)
        json.dump(config, f)  # dump generated config
    
    get_config()  # update config


# Output
def output(message: str = None, fatal: bool = False, help: bool = False) -> None:
    if fatal:
        print(f"{FATAL} {message}")
    elif help:
        print(f"[{BRIGHT_GREEN}HELP{RESET}] {message}")
    else:
        print(f"{PACKER} {message}")


# Init
def init() -> None:
    output("finding config file . . . ")
    
    if not os.path.isfile("./config.json"):
        output("Config not detected, creating")
        create_config()
        output("Created, initializing again")
        init()  # try to init again
    else:
        output("File found!")


# Help
def args_help() -> None:
    output("commands: pack, run, init", help = True)
    output("help: help prompt", help = True)
    output("init: init the program", help = True)
    output("pack: pack all files (compile to class path)", help = True)
    output("run: run a given program (if none is provided, then run entry point)", help = True)


# Arguments
def handle_args(args: list) -> None:
    global auto_retry

    if args[0] == "init":
        init()
    
    elif args[0] == "pack":
        try:
            get_config() # update conf
        except FileNotFoundError: # no config
            output("config was not found, please create before using the program. './p.py init' ", True)
            sys.exit(1)
       
        output("packing")
        
        retry_count = 0
        return_code = False
        failed_files = []
        reason = ""
        
        while retry_count is not True:
            try:
                return_code, failed_files, fail_pass_count, reason = packer.pack(args[1])
            except IndexError as i:
                return_code, failed_files, fail_pass_count, reason = packer.pack(failed_files if len(failed_files) != 0 else None)
        
                if failed_files is None: # prevent from looping
                    break

            if retry_count == 3 or auto_retry == False:
                if auto_retry == False:
                    pack_failed(fail_pass_count, failed_files, "Auto retry on fail was disabled, exited on Fail")
                else:
                    pack_failed(fail_pass_count, failed_files, reason)
                
                sys.exit(1)
            
            output("One or more files have failed to pack. Retrying . . .")
            retry_count += 1

        output(f"Report:    {BRIGHT_GREEN}PACKED{RESET} {fail_pass_count[0]}  {BRIGHT_RED}FAILED{RESET} {fail_pass_count[1]}")
    
    elif args[0] == "run":
        try:
            get_config()  # update conf
        except FileNotFoundError:  # no config
            output("config was not found, please create before using the program. './p.py init' ", True)
            sys.exit(1)
        
        output("Running")
        
        try:
            packer.run(args[1])
        except IndexError as i:
            packer.run()
    
    elif args[0] == "help" or args[0] == "-h":
        args_help()

    else:
        invalid_args(sys.argv)


if __name__ == "__main__":
    if len(sys.argv) != 1:
        handle_args(sys.argv[1:])  # exclude argv[0]
